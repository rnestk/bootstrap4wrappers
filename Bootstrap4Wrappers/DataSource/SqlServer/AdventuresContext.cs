namespace Bootstrap4Wrappers.DataSource.SqlServer
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Bootstrap4Wrappers.Models;

    public partial class AdventuresContext : DbContext
    {
        public AdventuresContext()
            : base("name=AdventuresDb")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<AdventuresContext>());
        }

        public virtual DbSet<Person> Person { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PersonConfiguration());
                
        }
    }
}
