﻿

$(document).ready(function () {

    $(document).ajaxStart(function () {
        $('#main-loader').show();
        $('#main-overlay-box').show();
    });
    $(document).ajaxStop(function () {
        $('#main-loader').hide();
        $('#main-overlay-box').hide();
    });

});