﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap4Wrappers.ViewModels
{
    public abstract class PagerViewModel<TEntity> : IPagerViewModel<TEntity> where TEntity : new()
    {
        public IEnumerable<TEntity> Elements { get; set; }
        public int ElementsTotalCount { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public string SortOrder { get; set; }
        public string PropertyName { get; set; }

        public string Search { get; set; }

        public TEntity this[int index]
        { get
            {
                return Elements.ElementAt(index);
            }
           
        }
    }
}