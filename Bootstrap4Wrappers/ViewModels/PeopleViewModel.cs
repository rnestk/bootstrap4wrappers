﻿using Bootstrap4Wrappers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap4Wrappers.ViewModels
{
    public class PeopleViewModel : PagerViewModel<Person>
    {
        public PeopleViewModel()
        {
            Page = 1;
            PageSize = 5;
        }
    }
}