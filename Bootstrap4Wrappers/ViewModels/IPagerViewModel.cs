﻿using System.Collections.Generic;

namespace Bootstrap4Wrappers.ViewModels
{
    public interface IPagerViewModel<TEntity> where TEntity : new()
    {
        TEntity this[int index] { get; }

        IEnumerable<TEntity> Elements { get; set; }
        int ElementsTotalCount { get; set; }
        int Page { get; set; }
        int PageSize { get; set; }
        string PropertyName { get; set; }
        string Search { get; set; }
        string SortOrder { get; set; }
    }
}