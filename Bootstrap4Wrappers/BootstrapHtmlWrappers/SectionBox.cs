﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bootstrap4Wrappers
{
    public class SectionBox : Box
    {
        public SectionBox()
        {
            _builder = new TagBuilder("section");
        }
    }
}