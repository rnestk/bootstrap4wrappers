﻿using Bootstrap4Wrappers.BootstrapHtmlWrappers.Standalone;
using Bootstrap4Wrappers.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bootstrap4Wrappers
{
    public static class InlineWrapper
    {

        public static UI UI(this HtmlHelper helper)
        {
            return new UI(helper.ViewContext.Writer);
        }

        
    }


    public class UI : IDisposable
    {
        protected TextWriter _writer;
        protected IBox _box;
        public IBox Box { get { return _box; } }
        public UI(TextWriter textWriter)
        {
            _writer = textWriter;
        }
        public UI Begin(IBox box)
        {
            _box = box;
            _writer.Write(box.GetStartTag());
            return this;
        }

        public Table<TSource> Table<TSource>(PagerViewModel<TSource> model) where TSource : new()
        {
            return new Table<TSource>(model);
        }

        public void Dispose()
        {
            _writer.Write(_box.GetEndTag());
        }
    }




}