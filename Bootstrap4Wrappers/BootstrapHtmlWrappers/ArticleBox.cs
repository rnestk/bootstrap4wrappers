﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap4Wrappers
{
    public class ArticleBox : Box
    {
        public ArticleBox()
        {
            _builder = new System.Web.Mvc.TagBuilder("article");
        }
    }
}