﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bootstrap4Wrappers.BootstrapHtmlWrappers.Standalone.Table
{
    public class Button : TagBuilder
    {
        public Button(string tag) : base(tag)
        {
        }

        public string Id { get; set; }
        public int Number { get; set; }
        public bool IsActive { get; set; }
        public string CssClass { get; set; }


        public override string ToString()
        {
            MergeAttribute("id", Id);
            MergeAttribute("data-state", IsActive ? "1" : "0");
            AddCssClass(CssClass);
            InnerHtml = Number.ToString();
            return base.ToString();
        }
    }
}