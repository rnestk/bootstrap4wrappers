﻿using Bootstrap4Wrappers.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bootstrap4Wrappers.BootstrapHtmlWrappers.Standalone.Table
{
    public class Caption<TEntity> : IHtmlString where TEntity: new()
    {
        protected TagBuilder _wrapper;
        protected string _title;
        protected PagerViewModel<TEntity> _model;
        protected string _tableId;
        protected bool _renderSearch;
        
        public string Title { get { return _title; } set { _title = value; } }

        public Caption(string title, PagerViewModel<TEntity> model, string tableId, bool isSearchable) 
        {
            _title = title;
            _model = model;
            _tableId = tableId;
            _renderSearch = isSearchable;
            _wrapper = new TagBuilder("div");
            _wrapper.AddCssClass("row table-caption");
        }

        public string[] SearchFields { get; set; }

        public string ToHtmlString()
        {
            TagBuilder col1 = new TagBuilder("div");
            col1.AddCssClass("col-12 title-box");
            TagBuilder col2 = new TagBuilder("div");
            col2.AddCssClass("col-12 search-box");
            TagBuilder p = new TagBuilder("p");
            p.InnerHtml += _title;
            col1.InnerHtml += p.ToString();
            col2.InnerHtml += HandleSearchHtml();
            _wrapper.InnerHtml += col1.ToString();
            _wrapper.InnerHtml += col2.ToString();
            return _wrapper.ToString();
        }

        private string HandleSearchHtml()
        {
            if(_renderSearch)
            {
                string placeholder = "";
                if (SearchFields.Any())
                {
                    placeholder = string.Join(", ", SearchFields);
                }

                string icon = "<i class='fas fa-search'></i>";
                TagBuilder search = new TagBuilder("a");
                search.AddCssClass("search-button");
                search.MergeAttribute("href", "#");
                search.MergeAttribute("data-page", _model.Page.ToString());
                search.MergeAttribute("data-sort", _model.SortOrder);
                search.MergeAttribute("data-property", _model.PropertyName);
                search.MergeAttribute("data-search", _model.Search);
                search.MergeAttribute("data-input", _tableId);
                search.InnerHtml += icon;
                TagBuilder input = new TagBuilder("input");
                input.AddCssClass("search-input");
                input.MergeAttribute("id", "search_" + _tableId);
                input.MergeAttribute("name", _tableId);
                input.MergeAttribute("placeholder", placeholder);
                input.MergeAttribute("value", _model.Search);
                
                return input.ToString() + search.ToString();
            }
            return "";
        }

        public override string ToString()
        {
            return ToHtmlString();
        }
    }
}