﻿using Bootstrap4Wrappers.BootstrapHtmlWrappers.Standalone.Table;
using Bootstrap4Wrappers.Core;
using Bootstrap4Wrappers.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Bootstrap4Wrappers.BootstrapHtmlWrappers.Standalone
{
    public class Table<TSource> : IHtmlString where TSource : new()
    {
        protected PagerViewModel<TSource> _model;
        protected TagBuilder _tableBox;
        protected TagBuilder _tableBuilder;
        protected TagBuilder _headerBuilder;
        protected TagBuilder _bodyBuilder;
        protected TagBuilder _footerBuilder;
        protected TagBuilder _paginationBuilder;
        protected TagBuilder _captionBuilder;
        protected string _id;
        protected IList<string> _classList;
        protected IList<Column> _columns;
        protected IList<Column> _additionalColumns;
        protected IList<object> _source;
        protected IList<List<string>> _rows;
        protected int _columnCount;
        protected int _rowIndex;
        protected bool _isPager;
        protected int _totalCount;
        protected Pagination<TSource> _pagination;
        protected Caption<TSource> _caption;
        protected string _title;
        protected bool _isSortable;
        protected bool _isSearchable;
        protected string[] _searchFields;
        //protected object _style;

        public Table(PagerViewModel<TSource> model)
        {
            _model = model;
            _tableBox = new TagBuilder("div");
            _tableBuilder = new TagBuilder("table");
            _headerBuilder = new TagBuilder("thead");
            _bodyBuilder = new TagBuilder("tbody");
            _footerBuilder = new TagBuilder("tfoot");
            _captionBuilder = new TagBuilder("div");
            _columns = new List<Column>();
            _additionalColumns = new List<Column>();
            _rows = new List<List<string>>();
            _classList = new List<string>();
            _id = Guid.NewGuid().ToString();
            _columnCount = 0;
            _source = new List<object>();
            
        }

        public int RowIndex { get { return _rowIndex; } }

        public Table<TSource> Title(string title)
        {
            _title = title;
            return this;
        }

        public Table<TSource> Sortable()
        {
            _isSortable = true;
            if(_isSortable)
            {
                _pagination = new Pagination<TSource>(_model, _id);
            }
            return this;
        }

        public Table<TSource> Searchable(params string[] fields)
        {
            _isSearchable = true;
            _searchFields = fields;
            return this;
        }

        public Table<TSource> DataSource(IEnumerable<TSource> source) 
        {
            SetSource(source);
            return this;
        }

        public Table<TSource> Id(string id)
        {
            _id = id;
            return this;
        }

        public Table<TSource> AddClass(string cssClass)
        {
            _classList.Add(cssClass);
            return this;
        }

        public Table<TSource> Data(string dataAttributeKey, string value)
        {
            string key = dataAttributeKey.StartsWith("data-") ? dataAttributeKey : "data-" + dataAttributeKey;
            _tableBuilder.MergeAttribute(key, value);
            return this;
        }


        public Table<TSource> Column(string columnTitle, string displayName = null, string format = null)
        {
            int maxid = _columns.Any() ? _columns.Max(c => c.Id) : 0;
            _columns.Add(new Standalone.Column { Title = columnTitle, Id = ++maxid, DisplayName = displayName ?? columnTitle, Format = format });
            _columnCount++;
            return this;
        }

        public Table<TSource> Column<T>(Expression<Func<TSource, T>> exp, string displayName = null, string format = null) 
        {
            int maxid = _columns.Any() ? _columns.Max(c => c.Id) : 0;
            string columnTitle = DynamicHelper.GetMemberName(exp);
            
            _columns.Add(new Standalone.Column { Title = columnTitle, Id = ++maxid, DisplayName = displayName ?? columnTitle, Format = format });
            _columnCount++;
            return this;
        }

        public Table<TSource> Column(Expression<Func<TSource, object>> exp, string displayName = null, string format = null) 
        {
            int maxid = _columns.Any() ? _columns.Max(c => c.Id) : 0;
            string columnTitle = DynamicHelper.GetMemberName(exp);
            _columns.Add(new Standalone.Column { Title = columnTitle, Id = ++maxid, DisplayName = displayName ?? columnTitle, Format = format });
            _columnCount++;
            return this;
        }

        public Table<TSource> Pagination()
        {
            if(!_isSortable)
            {
                _isPager = true;
                _pagination = new Pagination<TSource>(_model, _id);
            }
            
            return this;
        }


        protected virtual void SetSource(IEnumerable<TSource> source) 
        {
            var sourceList = source.Cast<object>().ToList();
            sourceList.ForEach((elem) => 
            {
                _source.Add(elem);
            });
        }

        protected virtual IEnumerable<TSource> GetSource() 
        {
            return (IEnumerable<TSource>)_source;
        }

        protected virtual void AddColumns()
        {
            if (!_columns.Any()) return;
            TagBuilder tr = new TagBuilder("tr");

            foreach (var column in _columns)
            {
                TagBuilder td = new TagBuilder("td");
                td.MergeAttribute("id", "head_" + column.Id.ToString());
                td.MergeAttribute("data-title", column.Title?.ToString());

                TagBuilder p = new TagBuilder("p");
                p.InnerHtml += column.DisplayName;

                TagBuilder row = new TagBuilder("div");
                row.AddCssClass("row");
                TagBuilder colLeft = new TagBuilder("div");

                if(_isSortable)
                {
                    colLeft.AddCssClass("col-6");
                }
                else
                {
                    colLeft.AddCssClass("col-12");
                }
                
                colLeft.InnerHtml += p.ToString();

                if (_isSortable)
                {
                    TagBuilder colRight = new TagBuilder("div");
                    colRight.AddCssClass("col-6");

                    TagBuilder asc = new TagBuilder("a");
                    TagBuilder desc = new TagBuilder("a");

                    asc.MergeAttribute("href", "#");
                    desc.MergeAttribute("href", "#");

                    asc.MergeAttribute("title", "Sortowanie wstepujące");
                    desc.MergeAttribute("title", "Sortowanie zstepujące");

                    asc.MergeAttribute("data-column", _id);
                    desc.MergeAttribute("data-column", _id);

                    asc.MergeAttribute("data-property", _model.PropertyName);
                    asc.MergeAttribute("data-sort", "asc");
                    asc.MergeAttribute("data-search", _model.Search);
                    desc.MergeAttribute("data-property", _model.PropertyName);
                    desc.MergeAttribute("data-sort", "desc");

                    asc.MergeAttribute("data-page", _model.Page.ToString());
                    desc.MergeAttribute("data-page", _model.Page.ToString());

                    asc.InnerHtml += "<i class='far fa-caret-square-up'></i>";
                    desc.InnerHtml += "<i class='far fa-caret-square-down'></i>";
                    colRight.InnerHtml += asc.ToString();
                    colRight.InnerHtml += desc.ToString();

                    row.InnerHtml += colLeft.ToString();
                    row.InnerHtml += colRight.ToString();
                }
                else
                {
                    row.InnerHtml += colLeft.ToString();
                }           
                td.InnerHtml = row.ToString();
                tr.InnerHtml += td.ToString();
            }
            _headerBuilder.InnerHtml += tr.ToString();
        }

        protected virtual void AddDataSource()
        {
            if (!_source.Any() || !_columns.Any() ) return;
            foreach (var item in _source)
            {
                var cols = new List<string>();
                foreach (var column in _columns.OrderBy(c => c.Id))
                {
                    var type = item.GetType();
                    var col = type.GetProperty(column.Title);
                    if (null != col)
                    {
                        var value = col.GetValue(item, null);
                        if (null != value)
                        {
                            if(!string.IsNullOrEmpty(column.Format))
                            {
                                cols.Add(string.Format(column.Format, value));
                            }
                            else
                            {
                                cols.Add(value.ToString());
                            }
                            
                        }
                        else
                        {
                            cols.Add(string.Empty);
                        }
                    }
                }
                _rows.Add(cols);
                
            }
            
        }

       
        protected virtual void ProcessDataSource()
        {
            if (!_source.Any() || !_columns.Any()) return;
            foreach (var cols in _rows)
            {
                TagBuilder tr = new TagBuilder("tr");
                _rowIndex++;
                tr.MergeAttribute("id", _rowIndex.ToString());
                tr.MergeAttribute("data-table", _id);
                foreach (var item in cols)
                {
                    TagBuilder col = new TagBuilder("td");
                    col.InnerHtml = item;
                    tr.InnerHtml += col.ToString();
                }

                _bodyBuilder.InnerHtml += tr.ToString();
            }
        }

        private void ProcessAttributes()
        {
            _tableBuilder.MergeAttribute("id", _id);
            if(_classList.Any())
            {
                foreach (var cl in _classList)
                {
                    _tableBuilder.AddCssClass(cl);
                }
            }
            else
            {
                _tableBuilder.AddCssClass("table table-bordered table-striped");
            }
            
        }

        

        protected void HandleCaption()
        {
            _caption = new Caption<TSource>(_title ?? "Tabela", _model, _id, _isSearchable);
            _caption.SearchFields = _searchFields;
            _captionBuilder.InnerHtml += _caption.ToString();
        }

        public string ToHtmlString()
        {
            ProcessAttributes();
            AddColumns();
            AddDataSource();
            ProcessDataSource();
            HandleCaption();

            _tableBox.AddCssClass("table-outer-box");
            _tableBox.MergeAttribute("data-page", _model.Page.ToString());
            _tableBox.MergeAttribute("data-sort", _model.SortOrder);
            _tableBox.MergeAttribute("data-property", _model.PropertyName);
            _tableBox.MergeAttribute("data-search", _model.Search);

            _tableBuilder.InnerHtml += _captionBuilder.ToString();
            _tableBuilder.InnerHtml += _headerBuilder.ToString();
            _tableBuilder.InnerHtml += _bodyBuilder.ToString();

            if (null != _pagination)
            {
                TagBuilder tr = new TagBuilder("tr");
                TagBuilder td = new TagBuilder("td");
                td.MergeAttribute("colspan", _columnCount.ToString());
                td.InnerHtml += _pagination.ToString();
                tr.InnerHtml += td.ToString();
                _footerBuilder.InnerHtml += tr.ToString();
            }

            _tableBuilder.InnerHtml += _footerBuilder.ToString();
            _tableBox.InnerHtml = _tableBuilder.ToString();
            
            return _tableBox.ToString();
        }

        
    }
}