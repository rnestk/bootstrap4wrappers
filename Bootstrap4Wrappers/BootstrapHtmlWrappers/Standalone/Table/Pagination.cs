﻿using Bootstrap4Wrappers.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bootstrap4Wrappers.BootstrapHtmlWrappers.Standalone.Table
{
    public class Pagination<TEntity> : IHtmlString where TEntity : new()
    {
        public TagBuilder _buttonsBuilder { get; set; }
        public TagBuilder _pagerBuilder;
        protected IList<Button> _buttons { get; set; }
        protected double _buttonsCount;
        protected bool _isVisible;
        protected string _tableId;
        protected PagerViewModel<TEntity> _model;

        public Pagination(PagerViewModel<TEntity> model, string tableId)
        {
            _model = model;
            _buttonsBuilder = new TagBuilder("div");
            _pagerBuilder = new TagBuilder("div");
            _buttons = new List<Button>();
            _buttonsCount = CalculateButtonsCount();
            _tableId = tableId;

        }

        protected virtual int CalculateButtonsCount()
        {
            int count = 0;
            if (_model.ElementsTotalCount > _model.PageSize)
            {
                return (int)Math.Ceiling((double)_model.ElementsTotalCount / (double)_model.PageSize);
            }
            return count;
        }

        protected virtual void CreateButtons()
        {
            int offset = 5;
            int current = _model.Page;
            int start = current - offset;
            int end = current + offset;

            if (end > (int)_buttonsCount) end = (int)_buttonsCount;

            if (start > 1)
            {
                CreateButtonHtml(1, "pager-add-button");
            }

            for (int i = start; i <= end; i++)
            {
                if (i > 0)
                    CreateButtonHtml(i);
            }


            if (_buttonsCount > end)
            {
                CreateButtonHtml((int)_buttonsCount, "pager-add-button");
            }
        }

        protected void CreateButtonHtml(int i, string addClass = "")
        {
            Button a = new Button("a");
            a.Id = string.Format("button_", i);
            a.IsActive = true;
            a.Number = i;
            a.CssClass = "btn btn-info pager-button " + addClass;
            a.MergeAttribute("data-tableid", _tableId);
            a.MergeAttribute("data-page", i.ToString());
            a.MergeAttribute("data-property", _model.PropertyName);
            a.MergeAttribute("data-sort", _model.SortOrder);
            a.MergeAttribute("data-search", _model.Search);
            a.MergeAttribute("data-pager", _tableId);

            if (_model.Page == i) a.CssClass += " current";
            _buttonsBuilder.InnerHtml += a.ToString();
        }

        public string ToHtmlString()
        {
            CreateButtons();
            _pagerBuilder.InnerHtml += _buttonsBuilder.ToString();

            return _pagerBuilder.ToString();
        }

        public override string ToString()
        {
            return ToHtmlString();
        }
    }
}