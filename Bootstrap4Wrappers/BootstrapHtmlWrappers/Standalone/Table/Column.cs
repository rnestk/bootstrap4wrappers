﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap4Wrappers.BootstrapHtmlWrappers.Standalone
{
    public class Column
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string DisplayName { get; set; }
        public string Format { get; set; }
    }
}