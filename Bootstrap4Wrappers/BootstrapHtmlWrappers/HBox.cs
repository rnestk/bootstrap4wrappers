﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bootstrap4Wrappers
{

    public enum HeaderSize
    {
        H1, H2, H3, H4, H5
    }

    public class HBox : Box
    {
        public HBox(HeaderSize size = HeaderSize.H1)
        {
            string hSize = "h1";
            switch (size)
            {
                case HeaderSize.H1:
                    hSize = "h1";
                    break;
                case HeaderSize.H2:
                    hSize = "h2";
                    break;
                case HeaderSize.H3:
                    hSize = "h3";
                    break;
                case HeaderSize.H4:
                    hSize = "h4";
                    break;
                case HeaderSize.H5:
                    hSize = "h5";
                    break;
                default:
                    hSize = "h1";
                    break;
            }
            _builder = new TagBuilder(hSize);
        }
    }
}