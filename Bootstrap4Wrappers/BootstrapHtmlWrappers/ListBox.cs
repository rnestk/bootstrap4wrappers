﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap4Wrappers
{
   
    public class ListBox : Box
    {
        public ListBox()
        {
            _builder = new System.Web.Mvc.TagBuilder("ol");
        }

    }

    public class ListElement : Box
    {
        public ListElement()
        {
            _builder = new System.Web.Mvc.TagBuilder("li");
        }
    }

}