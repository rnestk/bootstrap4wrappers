﻿using System;
using System.Web.Mvc;

namespace Bootstrap4Wrappers
{
    public interface IBox : IDisposable
    {
        MvcHtmlString GetEndTag();
        MvcHtmlString GetStartTag();
        IBox Id(string id);
        IBox AddClass(string cls);
        IBox AddData(string key, string value);
        IBox Style(string style);
        IBox Accesskey(string value);
        IBox ContentEditable(bool value);
        IBox Dir(string value);
        IBox Draggable(bool value);
        IBox Hidden(bool value);
        IBox Lang(string value);
        IBox Spellcheck(bool value);
        IBox Tabindex(string value);
        IBox Title(string value);

    }
}