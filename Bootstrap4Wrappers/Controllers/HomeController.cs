﻿using Bootstrap4Wrappers.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bootstrap4Wrappers.Core;
using ClosedXML.Excel;
using Bootstrap4Wrappers.Models;

namespace Bootstrap4Wrappers.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            IDataAccess<Person> dataProvider = null;

            dataProvider = new ExcelDataAccessProvider<Person>(Server.MapPath("~/DataSource/Excel/AdventureWorks2016.xlsx"));
            var people = dataProvider.GetAll();
            var vm = new PeopleViewModel();
            vm.PageSize = 25;
            people = people.Where(vm.Search, "FirstName", "LastName");

            vm.Elements = people
                .OrderBy(vm.PropertyName, vm.SortOrder)
                .Skip(vm.Page * vm.PageSize - vm.PageSize)
                .Take(vm.PageSize);
                
            vm.ElementsTotalCount = people.Count();
            return View(vm);
        }

        public ActionResult People(PeopleViewModel vm)
        {
            vm.PageSize = 25;
            int skip = (vm.Page * vm.PageSize) - vm.PageSize;
            var ep = new ExcelDataAccessProvider<Person>(Server.MapPath("~/DataSource/Excel/AdventureWorks2016.xlsx"));
            var people = ep.GetAll();
            people = people.Where(vm.Search, "FirstName", "LastName");

            vm.Elements = people
                .OrderBy(vm.PropertyName, vm.SortOrder)
                .Skip(skip)
                .Take(vm.PageSize);
            vm.ElementsTotalCount = people.Count();
            return PartialView(vm);
        }

        public ActionResult About()
        {
            
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        
    }
}