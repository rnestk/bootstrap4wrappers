﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bootstrap4Wrappers.Core
{
    public interface IDataAccess<TEntity> where TEntity : new()
    {
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();

        Task<TEntity> GetByIdAsync(int id);

        Task<IEnumerable<TEntity>> GetAllAsync();

        int Update(TEntity entity);

        Task<int> UpdateAsync(TEntity entity);

        int Add(TEntity entity);

        Task<int> AddAsync(TEntity entity);

        int Delete(TEntity entity);
        Task<int> DeleteAsync(TEntity entity);

    }
}