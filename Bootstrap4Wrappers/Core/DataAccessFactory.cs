﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bootstrap4Wrappers.Core
{
    public class DataAccessFactory<TEntity> where TEntity : new()
    {
        public IDataAccess<TEntity> CreateExcelDataProvider(string path)
        {
            return new ExcelDataAccessProvider<TEntity>(path);
        }

        public IDataAccess<TEntity> CreateSqlServerDataProvider()
        {
            return new SqlServerDataAccessProvider<TEntity>();
        }

        public IDataAccess<TEntity> CreateMySqlServerDataProvider()
        {
            return new MySqlDataAccessProvider<TEntity>();
        }
    }
}