﻿using Bootstrap4Wrappers.Models;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bootstrap4Wrappers.Core
{
    public class ExcelDataAccessProvider<TEntity> : IDataAccess<TEntity> where TEntity: new()
    {
        protected string _filePath;
        protected IXLWorkbook _workbook;
        protected IList<IXLColumn> _columns;
        protected IList<IXLRow> _rows;
        public ExcelDataAccessProvider(string filepath)
        {
            _filePath = filepath;
            _workbook = new XLWorkbook(filepath);
            _columns = _workbook.Worksheets.First().ColumnsUsed().ToList();
            _rows = _workbook.Worksheets.First().Rows().ToList();
            var col = _columns.First().Cell(1);
            
        }

        public int Add(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> AddAsync(TEntity entity)
        {
            return await Task.Run(() => 
            {
                return 1;
            });
            
        }

        public int Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TEntity> GetAll()
        {
            Type eType = typeof(TEntity);
            foreach (var row in _rows.Where(r => r != _rows.First()))
            {
                var entity = new TEntity();
                foreach (var col in _columns)
                {
                    var property = eType.GetProperty(col.Cell(1).Value.ToString());
                    object cellValue = GetProperType(row.Cell(col.ColumnNumber())?.Value, property.PropertyType);
                        //Convert.ChangeType(row.Cell(col.ColumnNumber())?.Value, property.PropertyType);
                    property.SetValue(entity, cellValue);
                }
                yield return entity;
            }
        }

        private object GetProperType(object value, Type propertyType)
        {
            TypeCode typeCode = Type.GetTypeCode(propertyType);

            switch (typeCode)
            {
                case TypeCode.Boolean:
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.Char:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Single:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.String:
                    return Convert.ChangeType(value, propertyType);
                case TypeCode.Object:
                    if (propertyType == typeof(Guid))
                    {
                        Guid guid = Guid.Empty;
                        Guid.TryParse(value.ToString(), out guid);
                        return guid;
                    }
                    else
                    {
                        return Convert.ChangeType(value, propertyType);
                    }
                case TypeCode.DateTime:
                    DateTime dt = new DateTime();
                    DateTime.TryParse(value.ToString(), out dt);
                    return dt;
                default:
                    return null;
            }
        }

        public Task<IEnumerable<TEntity>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public TEntity GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Task<TEntity> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateAsync(TEntity entity)
        {
            throw new NotImplementedException();
        }

        protected void RetrieveColumnNames()
        {
            var first = _workbook.Worksheets.FirstOrDefault();
            if(null == first) throw new Exception("Invalid excel file structure");
            var firstRow = first.Rows().FirstOrDefault();
            if (null == firstRow) throw new Exception("Invalid excel file structure, check if column names are present");
            //_columns = firstRow.CellsUsed().ToList();
        }

        protected void RetrieveRows()
        {

        }

    }
}