﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Bootstrap4Wrappers.Core
{
    public static class Extension
    {
        public static IEnumerable<TSource> OrderBy<TSource>(this IEnumerable<TSource> source, string propertyName) where TSource : new()
        {
            if (string.IsNullOrEmpty(propertyName)) return source;
            Type type = typeof(TSource);
            Func<TSource, object> lambda = (s) =>
            {
                if (null == s) return new object();
                return
                Convert.ChangeType(type.GetProperty(propertyName)?.GetValue(s, null),
                type.GetProperty(propertyName)?.GetValue(s, null) == null ? new object().GetType() : type.GetProperty(propertyName)?.GetValue(s, null).GetType());
            };
            return source.OrderBy(lambda).ToList();
        }

        public static IEnumerable<TSource> OrderByDescending<TSource>(this IEnumerable<TSource> source, string propertyName) where TSource : new()
        {
            if (string.IsNullOrEmpty(propertyName)) return source;
            Type type = typeof(TSource);

            Func<TSource, object> lambda = (s) =>
            {
                if (null == s) return new object();
                return
                Convert.ChangeType(type.GetProperty(propertyName)?.GetValue(s, null),
                type.GetProperty(propertyName)?.GetValue(s, null) == null ? new object().GetType() : type.GetProperty(propertyName)?.GetValue(s, null).GetType());
            };
            return source.OrderByDescending(lambda).ToList();
        }

        public static IEnumerable<TSource> OrderBy<TSource>(this IEnumerable<TSource> source, string propertyName, string sortOrder) where TSource : new()
        {
            if (string.IsNullOrEmpty(propertyName)) return source;
            if (string.IsNullOrEmpty(sortOrder)) return source;
            Type type = typeof(TSource);

            Func<TSource, object> lambda = (s) =>
            {
                if (null == s) return new object();
                return
                Convert.ChangeType(type.GetProperty(propertyName)?.GetValue(s, null),
                type.GetProperty(propertyName)?.GetValue(s, null) == null ? new object().GetType() : type.GetProperty(propertyName)?.GetValue(s, null).GetType());
            };
            if(sortOrder.ToLower() == "desc")
            {
                return source.OrderByDescending(lambda).ToList();
            }
            return source.OrderBy(lambda).ToList();
        }

        public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, string search, params string[] properties)
        {
            if (string.IsNullOrEmpty(search)) return source;
            List<TSource> list = new List<TSource>();
            Type type = typeof(TSource);
            if(properties.Any())
            {
                foreach (var property in properties)
                {
                    Func<TSource, bool> predicate = (s) => { return (type.GetProperty(property).GetValue(s, null)?.ToString().ToLower()).Contains(search.ToLower()); };
                    var resultList = source.Where(predicate);
                    if(resultList.Any())
                    {
                        list.AddRange(resultList);
                    }
                }
            }
            
            return list;
        }


    }
}