﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

namespace Bootstrap4Wrappers.Core
{
    public class DynamicHelper
    {
        public static string GetPropertyName(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null).ToString();
        }

        public static T GetMemberValue<T>(Expression<Func<T>> member)
        {
            return member.Compile().Invoke();
        }

        public static string GetMemberName<T>(Expression<Func<T>> member)
        {
            if (member.Body is MemberExpression)
            {
                return ((MemberExpression)member.Body).Member.Name;
            }
            return null;
        }

        public static string GetMemberName<TModel, TProperty>(Expression<Func<TModel, TProperty>> member)
        {
            if (member.Body is MemberExpression)
            {
                return ((MemberExpression)member.Body).Member.Name;
            }
            if (member.Body is UnaryExpression)
            {
                var operand = ((UnaryExpression)member.Body).Operand;
                return ((MemberExpression)operand).Member.Name;
            }
            return null;
        }

        public static PropertyInfo GetPropertyInfoByPropertyName<T>(string propertyName)
        {
            return typeof(T).GetProperty(propertyName);
        }

        public static void SetPropertyValue<T, TValue>(T target, Expression<Func<T, TValue>> memberLamda, TValue value)
        {
            var memberSelectorExpression = memberLamda.Body as MemberExpression;
            if (memberSelectorExpression != null)
            {
                var property = memberSelectorExpression.Member as PropertyInfo;
                if (property != null)
                {
                    property.SetValue(target, value, null);
                }
            }
        }


    }
}